# FabRISC ISA

This the official repository for the FabRISC project.

FabRISC is an open-source, modular, RISC-like instruction set architecture for 32 and 64 bit microprocessors (8 and 16-bit are also possible too). Some features of this specifications are:

* variable length instructions of 2, 4 and 6 bytes.
* scalar and vector capabilities.
* atomic and transactional memory support.
* privileged architecture three different modes.
* classically virtualizable.
* performance monitoring counters as well as debugging registers.
* and more!

### Compiling from source

This repository simply holds the LaTeX source code to generate the PDF documents for the privileged and unprivileged specifications. The steps to compile are very simple and all is needed is to compile the "FabRISC_unprivileged_spec.tex" file in the "./src/unprivileged_spec" directory or  "FabRISC_privileged_spec.tex" file in the "./src/privileged_spec" directory with PDFLaTeX to generate the PDF documents.

#### Disclaimer

This is just a personal project used as a learning experience so don't expect anything ground breaking, however any kind of feedback is absolutely welcome and needed!

### NOTE

This project is currently migrating from LaTeX to [Typst](https://github.com/typst/typst)
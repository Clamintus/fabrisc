% OK
% UNPRIVILEGED SPEC
\section[ISA modules and parameters]{\huge\underline{ISA modules and parameters}}

    \vspace{10pt}

    This section is dedicated to provide an overview of the modular capabilities of the FabRISC ISA. A list of the modules and implementation-specific parameters will be presented shortly.

    \subsection[Module list]{\Large Module list}

        \vspace{10pt}

        Features and capabilities are packaged in modules which can be composed of instructions or other requirements such as registers, events, modes and more. There are no mandatory modules in this specification in order to maximize the flexibility, however, once a particular extension is chosen, the hardware must provide all the features and abstractions of said extension. The requirements for each and every module will be extensively explained in the following sections when required. The following, is a simple list of all modules:

    \begin{itemize}

        \item \textit{\textbf{Computational}: these modules include instructions that perform arithmetic or logic operations:}

        \begin{enumerate}[label=\arabic*)]

            \item \textit{\textbf{CISB}: Computational-integer-scalar-basic.}
            \item \textit{\textbf{CISA}: Computational-integer-scalar-advanced.}
            \item \textit{\textbf{CISC}: Computational-integer-scalar-comparisons.}
            \item \textit{\textbf{CIVB}: Computational-integer-vector-basic.}
            \item \textit{\textbf{CIVA}: Computational-integer-vector-advanced.}
            \item \textit{\textbf{CIVR}: Computational-integer-vector-reduction.}
            \item \textit{\textbf{CIVC}: Computational-integer-vector-comparisons.}
            \item \textit{\textbf{CFSB}: Computational-FP-scalar-basic.}
            \item \textit{\textbf{CFSA}: Computational-FP-scalar-advanced.}
            \item \textit{\textbf{CFSC}: Computational-FP-scalar-comparisons.}
            \item \textit{\textbf{CFVB}: Computational-FP-vector-basic.}
            \item \textit{\textbf{CFVA}: Computational-FP-vector-advanced.}
            \item \textit{\textbf{CFVR}: Computational-FP-vector-reduction.}
            \item \textit{\textbf{CFVC}: Computational-FP-vector-comparisons.}
            \item \textit{\textbf{CSMW}: Computational-scalar-multi-word.}
            \item \textit{\textbf{CCIB}: Computational-compressed-integer-basic.}
            \item \textit{\textbf{CCIA}: Computational-compressed-integer-advanced.}
            \item \textit{\textbf{CCFB}: Computational-compressed-FP-basic.}
            \item \textit{\textbf{CCFB}: Computational-compressed-FP-advanced.}

        \end{enumerate}

        \item \textit{\textbf{Data transfer}: these modules include instructions that perform memory load, store, register move and swap operations:}

        \begin{enumerate}[resume,label=\arabic*)]

            \item \textit{\textbf{DSB}: Data-scalar-basic.}
            \item \textit{\textbf{DSA}: Data-scalar-advanced.}
            \item \textit{\textbf{DVB}: Data-vector-basic.}
            \item \textit{\textbf{DVA}: Data-vector-advanced.}
            \item \textit{\textbf{DA}: Data-atomic.}
            \item \textit{\textbf{DB}: Data-block.}
            \item \textit{\textbf{DC}: Data-compressed.}

        \end{enumerate}

        \item \textit{\textbf{Flow transfer}: these modules include instructions that perform conditional branch, jump, call and return operations:}

        \begin{enumerate}[resume,label=\arabic*)]

            \item \textit{\textbf{FIB}: Flow-integer-basic.}
            \item \textit{\textbf{FIA}: Flow-integer-advanced.}
            \item \textit{\textbf{FFB}: Flow-FP-basic.}
            \item \textit{\textbf{FFA}: Flow-FP-advanced.}
            \item \textit{\textbf{MS}: Masking-scalar.}
            \item \textit{\textbf{MV}: Masking-vector.}
            \item \textit{\textbf{FCIB}: Flow-compressed-integer-basic.}
            \item \textit{\textbf{FCIA}: Flow-compressed-integer-advanced.}
            \item \textit{\textbf{FCF}: Flow-compressed-FP.}

        \end{enumerate}

        \item \textit{\textbf{System}: these modules include instructions that perform various system related operations:}

        \begin{enumerate}[resume,label=\arabic*)]

            \item \textit{\textbf{SB}: System-basic.}
            \item \textit{\textbf{SA}: System-advanced.}

        \end{enumerate}

        \item \textit{\textbf{Miscellaneous}: these modules include miscellaneous components such as extra instructions, register banks, events and more:}

        \begin{enumerate}[resume,label=\arabic*)]

            \item \textit{\textbf{TM}: Transactional-memory.}
            \item \textit{\textbf{FNC}: Fencing.}
            \item \textit{\textbf{EXC}: Exceptions.}
            \item \textit{\textbf{FLT}: Faults.}
            \item \textit{\textbf{IOINT}: IO-interrupts.}
            \item \textit{\textbf{IPCINT}: IPC-interrupts.}
            \item \textit{\textbf{DBGR}: Debugging-registers.}
            \item \textit{\textbf{PERFC}: Performance-counters.}
            \item \textit{\textbf{SUPER}: Supervisor privilege.}
            \item \textit{\textbf{USER}: User privilege.}

            \item \textit{\textbf{DALIGN}: Data-alignment.}
            \item \textit{\textbf{IALIGN}: Instruction-alignment.}

        \end{enumerate}

    \end{itemize}

    \noindent Modules that provide events and privilege levels, such as EXC, FLT, IOINT, IPCINT, SUPER, USER, DALIGN and IALIGN will be thoroughly explained and discussed in a separate document dedicated to the privileged specification of FabRISC. If the hardware designers choose to not implement any privileged capability, then the machine must always run in \textit{machine mode} which has complete and total control over the architectural resources. Events can still be implemented in the said machine mode, but it will be discussed in the privileged specification. \vspace{10pt}

    \par\noindent\rule{\textwidth}{0.4pt}
    \textit{I consider this modular approach to be a wise idea because it allows the hardware designers to only implement what they really want with high degree of granularity and little extra. The fact that there is no explicit mandatory subset of the ISA can help with specialized systems. With this, it becomes perfectly possible to create, for example, a floating-point only processor with no integer instructions to alleviate overheads and extra complexities. This method, however, makes silly things possible such as having no flow transfer or no memory operations, so this system kind of relies on the common sense of the hardware designers when it comes to realizing sensible microarchitectures. The hardware can inform the software of it's capabilities by transforming the above seen list into a simple checklist from the module 1 as the least significant bit, all the way to the last module as the most significant one. This number is then written into the ISAMOD parameter and the software can then read this parameter via the SYSINFO instruction.
    \vspace{10pt}\hfill\break
    The miscellaneous modules also contain the SUPER and the USER modules, which are responsible for giving the ISA different privilege levels (the supervisor is intended for virtualization support). The FNC, EXC, FLT, IOINT, IPCINT, DBGR and PERFC allow the implementation of event-driven computation which, in conjunction with the earlier mentioned modules, is what really helps in supporting modern operative systems as well as proper virtualization techniques.}
    \par\noindent\rule{\textwidth}{0.4pt}

    \subsection[Implementation-specific parameters]{\Large Implementation-specific parameters}

        \vspace{10pt}

        FabRISC also makes use of some implementation-specific microarchitecture parameters to clear potential misunderstandings in both the documentation and the running software, as well as making the ISA more general and extensible. These parameters, along with other information, must actually be stored internally in the shape of read-only registers so that programs can gather information about various characteristics of the system via dedicated operations such as the SYSINFO instruction (see sections xxx and xxx for more information). Some are:

        \begin{itemize}

            \item \textit{\textbf{Word Length} (WLEN): this 8-bit parameter indicates the natural scalar word length of the processor in bits. Possible values are listed below, while the others are simply not used and are considered as reserved:}

            \begin{enumerate}[start=0, label=\arabic*)]

                \item \textit{8-bits.}
                \item \textit{16-bits.}
                \item \textit{32-bits.}
                \item \textit{64-bits.}

            \end{enumerate}

            \item \textit{\textbf{Maximum Vector Length} (MXVL): this 8-bit parameter indicates the maximum vector length of the processor in bits. The default value of 0 must be used if the hart doesn't support any vector capability. Possible values are listed below, while the others are simply not used and are considered as reserved:}

            \begin{enumerate}[start=0, label=\arabic*)]

                \item \textit{no vector capability is supported.}
                \item \textit{16-bit for 8-bit processors $(WLEN = 0)$.}
                \item \textit{32-bit for 8 and 16-bit processors $(WLEN = 0, 1)$.}
                \item \textit{64-bit for 8, 16 and 32-bit processors $(WLEN = 0, 1, 2)$.}
                \item \textit{128-bit for 8, 16, 32 and 64-bit processors $(WLEN = 0, 1, 2, 3)$.}
                \item \textit{256-bit for 8, 16, 32 and 64-bit processors $(WLEN = 0, 1, 2, 3)$.}
                \item \textit{512-bit for 16, 32 and 64-bit processors $(WLEN = 1, 2, 3)$.}
                \item \textit{1024-bit for 32 and 64-bit processors $(WLEN = 2, 3)$.}
                \item \textit{2048-bit for 64-bit processors $(WLEN = 3)$.}

            \end{enumerate}

            % TODO: CLEN parameter here

            \item \textit{\textbf{ISA Modules} (ISAMOD): this 64-bit parameter indicates the implemented instruction set modules of the processor previously described. ISAMOD works as a checklist where each bit indicates the desired module: the least significant bit will be the first module, while the most significant bit will be the last module in the list. The remaining bits are reserved for future expansion as well as custom extensions.}

            \item \textit{\textbf{Unprivileged ISA Version} (UISAVER): this 16-bit parameter indicates the currently implemented unprivileged ISA version. UISAVER is subdivided into two bytes with the most significant byte representing the major and the least significant byte the minor version.}

            \item \textit{\textbf{Privileged ISA Version} (PISAVER): this 16-bit parameter indicates the currently implemented privileged ISA version. PISAVER is subdivided into two bytes with the most significant byte representing the major and the least significant byte the minor version.}

        \end{itemize}

    \par\noindent\rule{\textwidth}{0.4pt}
    \textit{This ISA allows quite large vector widths to accommodate more exotic and special microarchitectures as well as byte-level granularity. The size must be at least twice the WLEN up to a maximum of $(32 \cdot WLEN)$ bits. The reason for this limit is because some instructions that manipulate special registers, such as vector masks, can only operate on $(4 \cdot WLEN)$ bit registers maximum due to a limited 2-bit shift field that selects the destination portion (see section 9 for more information). Nevertheless, the highest possible size of 2048-bits is quite large even for vector-heavy specialist machines. Vector is also possible at low WLEN of 8 and 16-bits but it probably won't be the best idea because of the limited word length. I expect a MXVL of 128 to 512-bits to be the most used for general purpose microarchitectures such as CPUs because it gives a good boost in performance for data-independent code without hindering other aspects of the system such as power consumption, chip area or resource usage in FPGAs. For an out-of-order machine the reservation stations and the reorder buffer would already be quite sizeable at 512 bits, however, it's always possible to decouple the two pipelines in order to minimize power and resource usage if a wider MXVL is desired.
    \vspace{10pt}\hfill\break
    The purpose of the other parameters is to increase code compatibility by laying out in clear way the capabilities of the microarchitecture. They are also handy for writing a more general and broad documentation that applies to many different situations. Each particular hart must hold these parameters as read-only values in special purpose registers (SPRs) in a separate bank from the main register file (see section 6 for more information).}
    \par\noindent\rule{\textwidth}{0.4pt}

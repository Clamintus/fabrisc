% OK

% data alignment
% synchronization
    % LL, SC, CAS
    % transactions
% consistency
    % fences
    % consistency modes
% addressing modes

% UNPRIVILEGED SPEC
\section[Memory]{\huge\underline{Memory}}

    \vspace{10pt}

    This section is dedicated to the memory model used by FabRISC including data alignment, synchronization, consistency, as well as possible cache coherence suggestions.

    \subsection[Data alignment]{\Large Data alignment}

        \vspace{10pt}

        FabRISC, overall, treats the main memory and the MMIO regions as collections of byte-addressable locations in little-endian order with a range of \(2^{WLEN}\) addresses in total. The specification leaves to the hardware designers the choice of supporting aligned or unaligned memory accesses or both for data. If aligned is decided to be the only supported scheme, the hart must generate the MISA fault every time the constraint is violated. When it comes to instructions, it is mandatory to align the code at the 16-bit boundary which is the greatest common denominator between the instruction lengths and the fetch engines must, obviously, be able to provide instructions aligned in this way. If the hardware detects that the program counter isn't aligned for whatever reason, the MISI fault must be generated to notify the rest of the system. Detecting PC misalignment simply amounts to checking if the last bit is equal to zero, otherwise the fault is generated. Because instructions are aligned at the 2-byte boundary, the branch offsets can be shifted by one to the left, thus, doubling the range in terms of bytes.
        \vspace{10pt}\hfill\break
        The DALIGN and IALIGN modules must be implemented if the hardware doesn't support misaligned accesses. They provide the architecture with the MISA and MISI faults respectively and they can be implemented in unprivileged microarchitectures as well (consult the privileged specification document for more information). \vspace{10pt}

    \par\noindent\rule{\textwidth}{0.4pt}
    \textit{Alignment issues can arise when the processor wants to read or write an item whose size is greater than the smallest addressable thing. This problem is tricky to design hardware for, especially caches, because misaligned items can cross cache line boundaries as well as page boundaries. Alignment networks and more advanced caches are needed to support this, which can increase complexity and slow down the critical path too much for simple designs. For already complex multicore out-of-order superscalar machines, however, i believe that supporting unaligned accesses can be handy so that the software writer can make decisions freely without having to worry about this issue, potentially decreasing the memory footprint in datastructures with many irregular objects.}
    \par\noindent\rule{\textwidth}{0.4pt}

    \subsection[Synchronization]{\Large Synchronization}

        \vspace{10pt}

        FabRISC provides dedicated atomic instructions via the AM instruction module to achieve proper synchronization in order to protect critical sections and to avoid data races in threads that share memory with each other. The proposed instructions behave atomically and can be used to implement atomic \textit{read-modify-write} and \textit{test-and-set} operations for locks, semaphores and barriers. It is important to note that if the processor can only run one hart at any given moment, then this section can be skipped since the problem can be solved by the operative system (or by software in general). Below is a description of the atomic instructions (see section xxx for more information), which are divided in two categories:

        \begin{itemize}

            \item \textit{\textbf{Read-modify-write} instructions:}

            \begin{enumerate}[label=\arabic*)]

                \item \textit{\textbf{Load Linked} (LL) is an atomic memory operation that loads an item from memory into a register and performs a ``reservation'' of the fetched location. The reservation can simply be storing the address and size of the object into an internal transparent register controlled by the hardware and marking it as valid.}

                \item \textit{\textbf{Store Conditional} (SC) is an atomic instruction that stores an item from a register to the target memory location if and only if the reservation matches and is marked as valid, that is, the address and size are the same plus the valid bit set. In the case of a mismatch, or an invalidity, SC must not perform the store and must return a zero in its destination register as an indication of the failure. If SC succeeds, the item is written to memory, a one is returned into its register destination and all reservations must then be invalidated. From this point in time, the update to the desired variable in memory can be considered atomic.}

            \end{enumerate}

            \item \textit{\textbf{Test-and-set} instructions:}

            \begin{enumerate}[label=\arabic*)]

                \item \textit{\textbf{Compare and swap} (CAS) is an atomic instruction that conditionally and atomically swaps two values in memory if a particular and specified condition is met. The conditions are equality, less than or equal or strictly less than.}

            \end{enumerate}

        \end{itemize}

        \noindent The LL and SC pair, as briefly mentioned above, necessitates of a transparent register that is used to hold the address of the fetched element as well as its size. Once the reservation is completed and marked as valid, accesses performed by other cores must be monitored via the coherence protocol. If a memory write request to the matching address is snooped by another hart, then that particular reservation must be immediately invalidated. The hardware designers are free to decide how strict the pair can behave by either storing the logical address and invalidate when the context changes, or reserve the physical address to relax it behavior.
        \vspace{10pt}\hfill\break
        The CAS instruction, in order to be atomic, must perform all of its operations in the same memory request. This ensures that no other hart or device has access to the memory potentially changing the target variable in the middle of the operation. The request should be held just for the necessary amount of time for CAS to complete in order to minimize contentions and memory traffic.
        \vspace{10pt}\hfill\break
        FabRISC also provides optional instructions, via the TM module, to support basic transactional memory that can be employed instead of the above seen solutions to exploit parallelism in a more ``optimistic'' manner. Multiple transactions can happen in parallel as long as no conflict is detected by the hardware. when such situations occur the offended transaction must be aborted, that is, it must discard all the changes and restore the architectural state immediately before the start of the transaction itself. If a transaction detects no conflict it is allowed to commit the changes and the performed operations can be considered atomic. Transactions can be nested inside each other up to a depth of 255, beyond this, they must be all immediately aborted. For convention, if an event that causes the context to be changed traps the hart, all of its ongoing transactions must be immediately aborted (consult the privileged specification for more information).
        \vspace{10pt}\hfill\break
        The following are the proposed transactional memory instructions:

        \begin{enumerate}[label=\arabic*)]

            \item \textit{\textbf{Transaction Begin} (TBEG): causes the hart that executed this instruction to checkpoint its architectural state and start monitoring accesses by other harts via the coherence protocol as well as incrementing the nesting counter by one. This instruction effectively starts a transaction.}

            \item \textit{\textbf{Transaction End} (TEND): causes the hart that executed this instruction to stop monitoring accesses by other harts and commit the changes as well as decrementing the nesting counter by one. This instruction effectively concludes a transaction. The updates to memory can be considered atomic and permanent after the completion of this instruction.}

            \item \textit{\textbf{Transaction Abort} (TABT): causes the hart that executed this instruction to stop monitoring accesses by other harts as well as triggering a jump to the abort handler with the appropriate abort code. The hart must also restore its architectural state immediately before the latest TBEG as well as decrementing the transaction nesting level. This instruction effectively aborts all transactions at the specified depth or greater. The updates to the hart's cache will be discarded and the terminated transactions will look like as if they never executed in the first place.}

            \item \textit{\textbf{Transaction Check} (TCHK): causes the hart that executed this instruction to return, in a specified register, the status of the current running transactional execution. This instruction effectively checks if the hart is in a transaction as well as its depth.}

            \item \textit{\textbf{Abort handler return} (AHRET): simple instruction to execute when returning from an abort handler. This instruction will restore the program counter back to the beginning of the aborted transaction.}

        \end{enumerate}

        \noindent The TM module also includes the presence of \textit{abort codes} that can be used by the programmer to take appropriate actions in case the transaction was aborted. The proposed codes are:

        \begin{enumerate}[label=\arabic*)]

            \item \textit{\textbf{Event abort} (EABT): the current transaction was aborted because an interrupt or fault got triggered.}

            \item \textit{\textbf{Conflict abort} (CABT): the current transaction was aborted because a write on shared variables was detected by the coherence protocol.}

            \item \textit{\textbf{Replacement abort} (RABT): the current transaction was aborted because a cache line was evicted back to memory for not enough associativity.}

            \item \textit{\textbf{Size abort} (SABT): the current transaction was aborted because a cache line was evicted back to memory for not enough space.}

            \item \textit{\textbf{Depth underflow abort} (UABT): this abort code is only generated if a TEND instruction is executed and the depth counter is zero.}

            \item \textit{\textbf{Depth overflow abort} (OABT): the current transaction was aborted because it exceeded the upper transaction depth limit.}

        \end{enumerate}

        \noindent The abort code is simply a number that is written into a special register (consult the privileged specification for more information). Alongside this code, the hardware must also save the depth level of the canceled transaction to aid the abort handler. 

    \par\noindent\rule{\textwidth}{0.4pt}
    \textit{Memory synchronization is extremely important in order to make shared memory communication even work at all. The problem arises when a pool of data is shared among different processes or threads that compete for resources and concurrent access to this pool might result in erroneous behavior and must, therefore, be arbitrated. This zone is called ``critical section'' and special atomic primitives can be used to achieve this protection. Many different instruction families can be chosen such as ``Compare-and-swap'', ``Test-and-set'', ``Read-modify-write'' and others. I decided to provide in the ISA the LL and SC pairs, as described above, because of its advantages and popularity among other RISC-like instruction sets. Two important advantages of this pair is that it is pipeline friendly (LL acts as a load and SC acts as a store) compared to others that try to do both. Another advantage is the fact that the pair doesn't suffer from the ``ABA'' problem. It is important to note, however, that this atomic pair doesn't guarantee forward progress and weaker implementations can reduce this chance even more. The CAS atomic instruction, even though it suffers from the ABA problem, it guarantees forward progress, rendering this instruction stricter.
    \vspace{10pt}\hfill\break
    I decided to also provide basic transactional memory support because, in some situations, it can yield great performance compared to mutual exclusion without losing atomicity. This is completely optional and up to the hardware designers to implement or not simply because it can significantly complicate the design. Transactional memory seems to be promising in improving performance and ease of implementation when it comes to shared memory programs, but debates are still ongoing to decide which exact way of implementing is best.}
    \par\noindent\rule{\textwidth}{0.4pt}

    \subsection[Coherence]{\Large Coherence}

        \vspace{10pt}

        FabRISC leaves to the hardware designer the choice of which coherence system to implement. On multicore systems cache coherence must be ensured by choosing a coherence protocol and making sure that all the cores agree on the current sequence of accesses to the same memory location. That can be guaranteed by serializing the operations via the use of a shared bus or via a distributed directory and \textit{write-update} or \textit{write-invalidate} protocols can be employed without any issues. Software coherence can also be a valid option but it will rely on the programmer to explicitly flush or invalidate the cache of each core separately. Nevertheless, FabRISC provides, via the SA instruction module, implementation-dependent instructions, such as CACOP, that can be sent to the cache controller directly to manipulate its operation (see section 9 for more information). If the processor makes use of a separate instruction cache, potential complications can arise for self modifying code which can be solved by employing one of the above options. \vspace{10pt}

    \par\noindent\rule{\textwidth}{0.4pt}
    \textit{Cache coherence is a big topic and is hard to get right because it can hinder performance in both single core and multicore significantly. I decided to give as much freedom as possible to the designer of the system to pick the best solution that they see fit. Another aspect that could be important, if the software route is chosen, is the exposure to the underlying microarchitecture implementation to the programmer which can be yield unnecessary complications and confusions. Generally speaking though write-invalidate seems to be the standard approach in many modern designs because of the way it behaves in certain situations, especially when a process is moved to another core. Simple shared bus can be a good choice if the number of cores is small (lots of cores means lots of traffic), otherwise a directory based approach can be used to ensure that all the cores agree on the order of accesses. From this, the protocol can be picked: MSI, MESI, MOSI or MOESI, the latter being the most complex but most powerful. All the harts that map to the same core don't need to worry about coherence since the caches are shared between those harts. This argument holds true for whole cores that share bigger pools of cache, such as L2 or L3. Virtually accessed caches are sort of an exception because false sharing can be an issue needing extra hardware to cover this edge case.}
    \par\noindent\rule{\textwidth}{0.4pt}

    \subsection[Consistency]{\Large Consistency}

        \vspace{10pt}

        FabRISC utilizes a fully relaxed memory consistency model formally known as \textit{release consistency} that allows all possible orderings in order to give harts the freedom to reorder memory instructions to different addresses in any way they want. For debugging and specific situations the stricter \textit{sequential consistency} model can be utilized and the hart must be able to switch between the two at any time via a dedicated bit in the control and status register. Special instructions, called \textit{``fences''}, are provided by the FNC module to let the programmer impose an order on memory operations when the relaxed model is in use. If the hart doesn't reorder memory operations this module is not necessary and can be skipped. The proposed fencing instructions are:

        \begin{enumerate}[label=\arabic*)]

            \item \textit{\textbf{Fence Loads} (FNCL): this instruction forbids the hart to reorder any load type instruction across the fence.}

            \item \textit{\textbf{Fence Stores} (FNCS): this instruction forbids the hart to reorder any store type instruction across the fence.}

            \item \textit{\textbf{Fence Loads and Stores} (FNCLS): this instruction forbids the hart to reorder any load or store type instructions across the fence.}

            \item \textit{\textbf{Fence Instructions} (FNCI): this instruction signals that a modification of the instruction cache happened.}

        \end{enumerate}

        \noindent The fences can be used on any memory type instruction, including the LL and SC pair and CAS to forbid reordering when acquiring or releasing a lock for critical sections and barriers. Writes to portions of memory where the code is stored can be made effective by issuing a command to the cache controller via the dedicated implementation specific CACOP instruction as briefly discussed above. The FNC module also requires the ability for the hart to switch between the release consistency and the more stringent sequential consistency model via the some special state (consult the privileged specification for more information). \vspace{10pt}

    \par\noindent\rule{\textwidth}{0.4pt}
    \textit{The memory consistency model i wanted to utilize was a very relaxed model to allow all kinds of performance optimization to take place inside the system. However one has to provide some sort of restrictions, effectively special memory operations, to avoid absurd situations. Even with those restrictions debugging could be quite difficult because the program might behave very weirdly, so i decided to include the sequential model that forbids reordering of any kind of memory instruction. If a program is considered well synchronized (data race-free and all critical sections are protected) consistency becomes less of an issue because there will be no contention for resources and, therefore, the model can be completely relaxed without any side effects. Achieving this level of code quality is quite the challenge and so these consistency instructions can be employed in making sure that everything works out.}
    \par\noindent\rule{\textwidth}{0.4pt}

    \subsection[Memory addressing modes]{\Large Memory addressing modes}

        \vspace{10pt}

        FabRISC provides five powerful and advanced addressing modes for both vector and scalar pipelines, from simple register indirect all the way to vector gather and scatter operations. This helps the code to be more compact increasing the efficiency of instruction caches as well as reducing the overhead of ``bookkeeping'' instructions for more complex access patterns. The addressing modes are divided into two parts, the first for scalar operations and the second for vector operations:

        \begin{itemize}

            \item \textit{\textbf{Scalar addressing modes}: these modes can access a single element at a time. The element can be any integer or floating point type of 8, 16, 32 or 64 bits:}

            \begin{enumerate}[label=\arabic*)]

                \item \textit{\textbf{Immediate displacement}: \((mem[reg + imm] \leftrightarrow reg)\) the address is composed of the sum between a register acting as a pointer and a constant.}

                \item \textit{\textbf{Immediate indexed}: \((mem[reg + reg + imm] \leftrightarrow reg)\) the address is composed of the sum between two registers, one acting as a pointer while the other acting as the index, and a constant.}

                \item \textit{\textbf{Immediate scaled}: \((mem[reg + (reg \cdot imm)] \leftrightarrow reg)\) the address is composed of the sum between a register acting as a pointer and the product of the index with a constant.}

            \end{enumerate}

            \item \textit{\textbf{Vector addressing modes} these modes can access multiple elements at a time. The number of elements accessed is dictated by the VLEN bits stored in the flag register. The elements are all the same type either integer or floating point of 8, 16, 32 or 64 bits:}

            \begin{enumerate}[resume,label=\arabic*)]

                \item \textit{\textbf{Vector immediate displacement}: \((mem_i[reg + \mbox{ stride}(imm)] \leftrightarrow vreg_i)\) the address is composed of a single register used as a pointer. The constant specifies the stride of the access.}

                \item \textit{\textbf{Vector indexed (gather / scatter)}: \((mem_i[reg + vreg_i] \leftrightarrow vreg_i)\) the address is composed of the sum between a base pointer register and the $i^{th}$ index of the specified vector register.}

            \end{enumerate}

        \end{itemize}

        \noindent Some modes also provide the ability to auto-update the index or base pointer via \textit{post-increment} or \textit{pre-decrement} to help with sequential data structures (see section 7 for more information). \vspace{10pt}

    \par\noindent\rule{\textwidth}{0.4pt}
    \textit{Good addressing modes can help improve code density, legibility (though low-level assembly isn't red by humans all the time) as well as performance since they can be directly implemented in hardware with very little overhead. The modes that i decided to support are handy for a variety of datastructures especially sequential ones such as stacks and arrays thanks to the auto-update features. Any addressing mode that uses an immediate as an offset also has a register counterpart to allow for higher variability and control. Vector modes include the common strided, as well as gather and scatter which are particularly useful for sparse matrices.
    \vspace{10pt}\hfill\break
    Thanks to the modular nature of FabRISC, it's not required to implement the complex modes at all, only the immediate displacement sa it's the simplest and most commonly used in simpler implementations.}
    \par\noindent\rule{\textwidth}{0.4pt}
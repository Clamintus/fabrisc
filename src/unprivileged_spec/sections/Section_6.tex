% UNPRIVILEGED SPEC
\section[ISA specification]{\huge\underline{ISA specification}}

    \vspace{10pt}

    In this section the register file organization, vector model, processor modes and the instruction formats are presented. Processors that don't support certain modules must generate the INCI fault whenever an unimplemented or incompatible instruction is fetched or attempted to be processed in any other way. Processors must also generate the ILLI fault whenever a combination of all zeros or all ones is fetched in order to increase safety against buffer overflows exploits or accidental accesses of uninitialized memory locations. The full instruction list will be presented separately in the next section as it's quite large.

    \subsection[Register file]{\Large Register file}

        \vspace{10pt}

        Depending on which modules are chosen as well as the WLEN and MXVL values, the register file can be composed of up to five different banks of variable width along with extra special purpose ones mainly used by the privileged specification. Registers are declared in the following presented list in order:

        \begin{enumerate}[label=\arabic*)]

            \item \textit{\textbf{Scalar general purpose registers} (SGPRs): this bank is composed of 32 registers which can be used to hold variables during program execution. The registers are all WLEN bits wide and are used by scalar integer and floating point instructions together. All SGPRs are not privileged and private for each hart.}

            \item \textit{\textbf{Vector general purpose registers} (VGPRs): this bank is composed of 32 registers which can be used to hold variables during program execution. The registers are all MXVL bits wide and are used by vector integer and floating point instructions together. This bank is only necessary if the machine in question supports any vector instructions. All VGPRs are not privileged and private for each hart.}

            \item \textit{\textbf{Debugging registers} (DBGRs): this bank is composed of 16 registers which can be used for debugging, automatic address boundary checking and triggering exceptions. This bank is only necessary if the machine in question supports the DBGR module. These registers are all WLEN bits wide, not privileged and private for each hart.}

            \item \textit{\textbf{Performance counters} (PERFCs): this bank is composed of 16 registers which can be used for performance diagnostic, timers and counters. This bank is only necessary if the machine in question supports the PERFC module. These registers are all CLEN bits wide, not privileged and private for each hart.}

            \item \textit{\textbf{Special purpose registers} (SPRs): this bank is introduced in this section, however, it will be discussed in great detail in the privileged specification because only a small part of these registers are needed here. Please check that document for more information.}

        \end{enumerate}

        \subsubsection[Register ABI]{\large Register ABI}

            \vspace{10pt}

            FabRISC specifies an ABI (application binary interface) for the SGPRs, VGPRs and HLPRs. It is important to note that this is just a suggestion on how the general purpose registers should be used in order to increase code compatibility. As far as the processor is concerned, all SGPRs and VGPRs behave in the same way. The register convention for SGPRs is as follows:

            \begin{itemize}

                \item \textit{\textbf{Parameter registers}: registers prefixed with the letter 'P' are used for parameter passing and returning to and from function calls. Parameters are stored in these registers starting from the top-down $(P_0 \rightarrow P_n)$, while returning values are stored starting from the bottom-up $(P_n \rightarrow P_0)$.}

                \item \textit{\textbf{Persistent registers}: registers prefixed with the letter 'S' are ``persistent'', that is, registers whose value should be retained across function calls. This implies a ``callee-save'' calling convention.}

                \item \textit{\textbf{Volatile registers}: registers prefixed with the letter 'N' are ``volatile'' or simply ``non persistent'', that is, registers whose value may not be retained across function calls. This implies a ``caller-save'' calling convention.}

                \item \textit{\textbf{Stack pointer} (SP): this register is used as a pointer to the call-stack. The calling convention for this register is callee-save.}

                \item \textit{\textbf{Return address} (RA): this register is used to hold the return address for the currently executing function. The calling convention for this register is caller-save.}

                \item \textit{\textbf{Global pointer} (GP): this register is used to point to the global variable area and is always accessible across calls. There is no calling convention for this register.}

            \end{itemize}

            \noindent Vector registers are all considered volatile, which means that the caller-save scheme must be utilized since it's assumed that their value won't be retained across function calls. Special instructions are also provided to move these registers, or part of them, to and from the SGPRs (see section 7 for more information).
            \vspace{10pt}\hfill \break
            Some compressed instructions are limited in the number of registers that they can address due to their reduced size. The specifiers are 3-bit long allowing the top eight registers to be addressed only. The proposed ABI already accounts for this by placing the most important eight registers at the top of the bank.

        \subsubsection[Debugging register bank]{\large Debugging register bank}

            \vspace{10pt}

            This bank houses the DBGRs which, as mentioned earlier, can be used for debugging, address range checks and triggering exceptions. These registers are all WLEN bits wide and, depending on the value written in the DBGMs, their operating mode can be programmed. The DBGRs require the implementation of this bank, some special instructions (see section 7 for more information), some exception events (see the privileged specification document for more information) as well as the DBGRs themselves. The operating modes are the following and must be stored as an 8-bit code for each DBGR:

            \begin{enumerate}[start=0, label=\arabic*)]

                \item \textit{\textbf{Disabled}: this mode doesn't do anything.}

                \item \textit{\textbf{Break on execute address}: this mode will cause the corresponding HLPR register to generate the EXT exception as soon as the hart tries to fetch the instruction at the specified address.}

                \item \textit{\textbf{Break on read address}: this mode will cause the corresponding HLPR register to generate the RDT exception as soon as the hart tries to read data at the specified address.}

                \item \textit{\textbf{Break on write address}: this mode will cause the corresponding HLPR register to generate the WRT exception as soon as the hart tries to write data at the specified address.}

                \item \textit{\textbf{Break on read or write address}: this mode will cause the corresponding HLPR register to generate the RWT exception as soon as the hart tries to read or write data at the specified address.}

                \item \textit{\textbf{Break on read or write or execute address}: this mode will cause the corresponding HLPR register to generate the RWET exception as soon as the hart tries to read or write data or fetch the instruction at the specified address.}

                \item \textit{\textbf{Break on execute range}: this mode will cause the corresponding HLPR register to generate the EXRT exception as soon as the hart tries to fetch instructions outside the specified range. If this mode is selected, the value of the specified HLPR register will be considered the starting address of the range. The terminating address of the range will be held in the HLPR register after the specified one and its mode will be ignored.}

                \item \textit{\textbf{Break on read range}: this mode will cause the corresponding HLPR register to generate the RDRT exception as soon as the hart tries to read data outside the specified range. If this mode is selected, the value of the specified HLPR register will be considered the starting address of the range. The terminating address of the range will be held in the HLPR register after the specified one and its mode will be ignored.}

                \item \textit{\textbf{Break on write range}: this mode will cause the corresponding HLPR register to generate the WRRT exception as soon as the hart tries to write data outside the specified range. If this mode is selected, the value of the specified HLPR register will be considered the starting address of the range. The terminating address of the range will be held in the HLPR register after the specified one and its mode will be ignored.}

                \item \textit{\textbf{Break on read or write range}: this mode will cause the corresponding HLPR register to generate the RWRT exception as soon as the hart tries to read or write data outside the specified range. If this mode is selected, the value of the specified HLPR register will be considered the starting address of the range. The terminating address of the range will be held in the HLPR register after the specified one and its mode will be ignored.}

                \item \textit{\textbf{Break on read or write or execute range}: this mode will cause the corresponding HLPR register to generate the RWERT exception as soon as the hart tries to read or write data or fetch instructions outside the specified range. If this mode is selected, the value of the specified HLPR register will be considered the starting address of the range. The terminating address of the range will be held in the HLPR register after the specified one and its mode will be ignored.}

                \item \textit{\textbf{Break on COVR1 flag}: this mode will cause the corresponding HLPR register to generate the COVR1T exception as soon as the COVR1 flag is raised at the instruction address held in the specified HLPR register.}

                \item \textit{\textbf{Break on COVR2 flag}: this mode will cause the corresponding HLPR register to generate the COVR2T exception as soon as the COVR2 flag is raised at the instruction address held in the specified HLPR register.}

                \item \textit{\textbf{Break on COVR4 flag}: this mode will cause the corresponding HLPR register to generate the COVR4T exception as soon as the COVR4 flag is raised at the instruction address held in the specified HLPR register.}

                \item \textit{\textbf{Break on COVR8 flag}: this mode will cause the corresponding HLPR register to generate the COVR8T exception as soon as the COVR8 flag is raised at the instruction address held in the specified HLPR register.}

                \item \textit{\textbf{Break on CUND flag}: this mode will cause the corresponding HLPR register to generate the CUNDT exception as soon as the CUND flag is raised at the instruction address held in the specified HLPR register.}

                \item \textit{\textbf{Break on OVFL1 flag}: this mode will cause the corresponding HLPR register to generate the OVFL1T exception as soon as the OVFL1 flag is raised at the instruction address held in the specified HLPR register.}
                
                \item \textit{\textbf{Break on OVFL2 flag}: this mode will cause the corresponding HLPR register to generate the OVFL2T exception as soon as the OVFL2 flag is raised at the instruction address held in the specified HLPR register.}

                \item \textit{\textbf{Break on OVFL4 flag}: this mode will cause the corresponding HLPR register to generate the OVFL4T exception as soon as the OVFL4 flag is raised at the instruction address held in the specified HLPR register.}

                \item \textit{\textbf{Break on OVFL8 flag}: this mode will cause the corresponding HLPR register to generate the OVFL8T exception as soon as the OVFL8 flag is raised at the instruction address held in the specified HLPR register.}

                \item \textit{\textbf{Break on UNFL1 flag}: this mode will cause the corresponding HLPR register to generate the UNFL1T exception as soon as the UNFL1 flag is raised at the instruction address held in the specified HLPR register.}

                \item \textit{\textbf{Break on UNFL2 flag}: this mode will cause the corresponding HLPR register to generate the UNFL2T exception as soon as the UNFL2 flag is raised at the instruction address held in the specified HLPR register.}

                \item \textit{\textbf{Break on UNFL4 flag}: this mode will cause the corresponding HLPR register to generate the UNFL4T exception as soon as the UNFL4 flag is raised at the instruction address held in the specified HLPR register.}

                \item \textit{\textbf{Break on UNFL8 flag}: this mode will cause the corresponding HLPR register to generate the UNFL8T exception as soon as the UNFL8 flag is raised at the instruction address held in the specified HLPR register.}

                \item \textit{\textbf{Break on DIV0 flag}: this mode will cause the corresponding HLPR register to generate the DIV0T exception as soon as the DIV0 flag is raised at the instruction address held in the specified HLPR register.}

                \item \textit{the remaining modes are reserved for future use.}

            \end{enumerate}

            \noindent If multiple DBGRs specify ranges, those ranges must be AND-ed together in order to allow proper automatic checking of things such as memory address ranges.
            \vspace{10pt}\hfill \break
            DBGRs that are disabled can still be red and written and utilized at any moment with a caller-save scheme, which simply means, that they are considered as volatile.
            \vspace{10pt}\hfill \break
            If multiple same events are triggered in the same cycle, then they must be queued to avoid loss of information. In order to avoid any ambiguity when such situations arise, the ordering convention gives the DBGR0 the highest priority and DBGR15 the lowest.

        \subsubsection[Performance counters bank]{\large Performance counters bank}

            \vspace{10pt}

            This bank houses the PERFCs which, as mentioned earlier, can be used for performance diagnostic, timers and counters. These registers are all CLEN bits wide and, depending on the value written in the PERFCMs, their operating mode can be programmed. The PERFCs require the implementation of this bank, some special instructions (see section 7 for more information), some exception events (see the privileged specification document for more information) as well as the PERFCs themselves. The operating modes are the following and must be stored as a 8-bit code for each PERFC:

            \begin{enumerate}[start=0, label=\arabic*)]

                \item \textit{\textbf{Disabled}: this mode doesn't do anything.}

                \item \textit{\textbf{Clock counter}: this mode is a simple clock cycle counter that increments with each passing tick.}

                \item \textit{\textbf{Instruction counter}: this mode is a simple instruction counter that increments with each retired instruction.}

                \item \textit{\textbf{Memory load counter}: this mode is a simple instruction counter that increments with each executed memory load instruction.}

                \item \textit{\textbf{Memory store counter}: this mode is a simple instruction counter that increments with each executed memory store instruction.}

                \item \textit{\textbf{Taken branch counter}: this mode, is a simple instruction counter that increments with each taken conditional branch.}

                \item \textit{\textbf{Non-taken branch counter}: this mode is a simple instruction counter that increments with each non-taken conditional branch.}

                \item \textit{the remaining modes are left as implementation specific.}

            \end{enumerate}

            \noindent If a PERFC overflows and the CNTE bit in the SR is set, then the CNTO exception is triggered signaling the overflow of the targeted counter. If multiple CNTO events are triggered in the same cycle, then they must be queued to avoid loss of information. In order to avoid any ambiguity when such situations arise, the ordering convention gives the PERFC0 the highest priority and PERFC15 the lowest.

            \vspace{10pt}
            \input{../data/tables/Register_file.tex}

        \subsubsection[Introduction to the special register bank]{\large Introduction to the special register bank}

            In this small subsection, some of the SPRs that do not concern with privileges are quickly discussed. These registers are considered \textit{global}, that is, always available to anyone regardless of the current privilege. SPRs are also \textit{hart private}, which means that, each hart must hold its own copy of these registers. The global SPRs are:

            \begin{itemize}

                \item \textit{\textbf{Program counter} (PC): this register is used to point to the currently executing instruction. PC is WLEN bits wide.}

                \item \textit{\textbf{Status register} (SR): this register is used to hold simple state for the currently running hart. SR is xxx bits wide.}

                \item \textit{\textbf{Vector masks}: registers prefixed with ``VMSK'' are used to hold the vector masks. A single bit signifies a mask for its corresponding byte of the vector result. Dedicated instructions are provided to directly set and manipulate the value of the VMSK registers. The VMSK registers are \((MXVL / 8)\) bits wide and are optional depending on vector implementation if any (see the next section for more information).}

                \item \textit{\textbf{Debugging registers mode}: registers prefixed with ``DBGRM'' are dedicated to store the 8-bit mode code of the DBGRs. There are two DBGRMs and each is segmented into eight 8-bit chunks. DBGRM0 holds the codes for \(DBGR0 \rightarrow DBGR7\) and DBGRM1 holds the codes for \(DBGR8 \rightarrow DBGR15\). the DBGRM registers are 64-bits wide and are optional depending on the presence of the DBGR module or not.}

                \item \textit{\textbf{Performance counters mode}: registers prefixed with ``PERFCM'' are dedicated to store the 8-bit mode code of the PERFCs. There are three PERFCMs and each is segmented into eight 8-bit chunks. PERFCM0 holds the codes for \(PERFC0 \rightarrow PERFC7\) and PERFCM1 holds the codes for \(PERFC8 \rightarrow PERFC15\). the PERFCM registers are 64-bits wide and are optional depending on the presence of the PERFC module or not.}

                % others if any...

            \end{itemize}
            \vspace{10pt}

            \input{../data/tables/Unprivileged_special_registers.tex}

        \subsubsection[Status register bit layout]{\large Status register bit layout}

            The SR register is divided into several bit and flag fields in order to provide granular control over the common system state. If the microarchitecture doesn't support certain features, then the corresponding bits for those features can be ignored and set to zero for convention. The bits are listed from 0 to xxx in the order that they appear:

            \begin{itemize}

                \item \textit{\textbf{Vector length} (VLEN): eight bits that indicate how many elements a vector instruction will operate on. A value of zero will signify only one element, while a value of 255 will signify all of the elements.}

                \item \textit{\textbf{Rounding modes} (RMD): three bits that specify the floating point rounding modes which can be:}

                \begin{enumerate}

                    \item \textit{$\rightarrow$ towards $+\infty$}
                    \item \textit{$\rightarrow$ towards $-\infty$}
                    \item \textit{$\rightarrow$ towards zero.}
                    \item \textit{$\rightarrow$ towards even.}
                    \item \textit{The remaining combinations are reserved for future use.}

                \end{enumerate}

                \item \textit{\textbf{Scalar masks} (SMSKs): three bits that determine the mask for scalar instructions. A value of zero will allow the result to be written, while a value of one will prevent the instruction to alter any state. The masks behave as single bit register, for a total capacity of three masks.}

                % others if any...

            \end{itemize}

        \subsubsection[Wide register manipulation]{\large Wide register manipulation}

            \vspace{10pt}

            Registers that are wider than WLEN pose a problem when it comes to instructions that can manipulate up to WLEN bits especially on 8 or 16-bit microarchitectures with some special registers. Some instructions have a 2-bit shift amount that allows the programmer to choose which portion of the destination register to act on.
            \vspace{10pt}

    \par\noindent\rule{\textwidth}{0.4pt}
    \textit{The entirety of the register file is divided into five banks with several registers each. This can sound like a lot of state (and it kind of is), but other ISAs such as full RISC-V implementation would perhaps contain more because of the way CSRs are implemented. In FabRISC, the first bank offers a 32 entry flat register file in order to efficiently support modern graph-coloring driven compiler register allocation. Integer and floating-point registers are shared to ease conversions and extra GPRs can be found in the special purpose and helper banks to potentially alleviate some of the pressure caused by the sharing (check the privileged specification document for more information). Because there is no hardware stack pointer nor return address register, standard calling conventions can be used to give a ``purpose'' to all the GPRs in the scalar bank. As the name suggests, this is simply a convention that can be changed at will without having to modify the underlying hardware.
    \vspace{10pt}\hfill\break
    Compressed instructions, because of the size limitations, are only able to access the top eight registers of the scalar bank. That may seem limiting, but registers usually have a lot of locality of reference thus allowing some corner cutting on the register addresses.
    \vspace{10pt}\hfill\break
    Global SPRs are useful to keep track of essential things such as, status, configuration flags, masks and more. These registers must always be available regardless of the currently running privilege.
    \vspace{10pt}\hfill\break
    The DBGRs and PERFCs are an extra useful tool to help with the development of software allowing breakpoint driven debugging, as well as performance monitoring for profilers. The DBGRs can also be handy in performing event-driven computation, such as invoking handlers when specific exceptions trigger, without having to perform time consuming checks especially on address ranges for memory safety.}
    \par\noindent\rule{\textwidth}{0.4pt}

    \subsection[Instruction formats]{\Large Instruction formats}

        \vspace{10pt}

        FabRISC specifies 15 different instruction formats to allocate as much space as possible to the operands, while leaving some opcode encoding space to implement extra instructions. Formats can be variable length of two, four and six bytes in order to accommodate larger immediate constants for addresses and data. Formats are divided into several bit fields that are scrambled around in order to reduce decoding logic and fine tune performance. Many also carry with them a ``modifier'' field that can provide extra information such as a mask or extra immediate bits:

            \begin{itemize}

                \item \textit{\textbf{``mm''}: these bits indicate the mask to use for the instruction. A value of 11 signifies that no mask is desired, therefore, the instruction will always be executed.}

                \item \textit{\textbf{``ss''}: these bits indicate the section of the source register on which the instruction will work on. Mainly used in special instructions to move wide registers.}

                \item \textit{\textbf{``ii''}: these bits simply indicate extra most significant immediate bits.}
                \item \textit{\textbf{``-''}: this indicates an unused bit or bits. It must be zero for convention.}

            \end{itemize}

            \noindent The instruction modifier bit field, when present, depends on the particular format in question and are supposed to be fed directly to the target functional unit without further decoding:

            \begin{enumerate}[label=\arabic*)]

                \item \textit{\textbf{[ii]}: this class is used by the ... formats.}
                \item \textit{\textbf{[mm]}: this class is used by the ... formats.}

            \end{enumerate}

            \vspace{10pt}
            \noindent The following are two tables that present the proposed formats. The first table describes them in a more abstract fashion, that is, just listing their logical composition for the opcode and operands. The second table lists the actual bit patterns that are going to be interpreted by the underlying hardware:

            \input{../data/tables/Logical_formats_table.tex}
            % format bit patterns table

            \vspace{10pt}
            \noindent The following is the legend for both of the above tables:
            
            \begin{itemize}

                \item \textit{\textbf{op, opc}: short for instruction opcode.}
                \item \textit{\textbf{mod}: short for instruction modifier.}
                \item \textit{\textbf{ra, rb, rc, rd}: short for register a, b, c, d which are register specifiers.}
                \item \textit{\textbf{im, imm}: short for immediate.}
                \item \textit{\textbf{len, l}: short for length.}

            \end{itemize}

    % REWRITE THE COMMENT
    %\par\noindent\rule{\textwidth}{0.4pt}
    %\textit{The moderate number of formats arise from the fact that i wanted to ``best-fit'' several groups of instructions in order to leave as much space as possible to the operands (mainly immediates) without hindering the encoding space. I believe, however, that the decoding complexity is still very manageable due to the high degree of similarity among the formats because it's often just a few bits changing purpose or position. The register specifiers are always in the same position to reduce decoding complexity and improve performance by allowing the registers to be red while the instruction is finishing decoding in simpler designs. The first six bits are always the prefix, while the rest of the opcode bits are divided and positioned in convenient places to further simplify the control unit. Immediates are also scattered around the formats for the same complexity reason. The most significant bits of the immediates are not always in the same positions because it's a hard constraint to follow. The idea though, was to sign-extend constants in a second phase of the decoding because some instructions need zero-extended immediates, while others need sign-extended immediates.}
    %\par\noindent\rule{\textwidth}{0.4pt}

    \par\noindent\rule{\textwidth}{0.4pt}
    \textit{Comment goes here...}
    \par\noindent\rule{\textwidth}{0.4pt}
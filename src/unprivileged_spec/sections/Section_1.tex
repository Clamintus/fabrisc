% OK
% UNPRIVILEGED SPEC
\section[Introduction]{\huge\underline{Introduction}}

    \vspace{10pt}

    This is the official document for the unprivileged FabRISC instruction set architecture. This writing is intended to introduce and explain the basic concepts and core capabilities of the ISA. 
    \vspace{10pt}\hfill \break
    FabRISC is a feature rich, register-register, classically virtualizable, load-store architecture with variable length encodings of 2, 4 and 6 bytes. This specification is designed to be highly modular allowing simple and straightforward implementations from basic designs up to high performance systems by picking and choosing only the desired functionalities. The ISA includes scalar, vector, floating-point, compressed as well as atomic memory instructions capable of supporting 32 and 64-bit multithreaded microarchitectures and, with more basic implementations, it's also possible to realize processors of 8 and 16-bit widths as well. The complete specification also includes privileged and system related instructions, interrupts, exceptions, faults and more which help support the realization of various kinds of operative systems with or without memory management needs (see the privileged spec). The ISA can be further expanded and enriched by allocating unused encoding space to custom application specific instructions, thus increasing both performance and code density. FabRISC is completely free, open-source and available to anyone interested in the project via the official GitLab page: \url{https://gitlab.com/Clamintus/fabrisc} (license details can be found at the very end of this document). The specification is divided into multiple sections each explaining a particular aspect of the architecture in detail with the help of tables, figures and suggestions in order to aid the hardware and software designers in creating an efficient realization of the FabRISC instruction set architecture. \vspace{10pt}

    \par\noindent\rule{\textwidth}{0.4pt}
    \textit{Commentary in this document will formatted in this way and communication will be more colloquial. If the reader is only interested in the specification, these sections can be skipped without hindering the understanding of the document.
    \vspace{10pt}\hfill\break
    I decided to split the privileged and unprivileged specification in two separate documents for better decoupling. This is mainly to make changes on one specification be more independent of the other, as well as to help me getting things done more quickly.
    \vspace{10pt}\hfill\break
    This project tries to be more of a hobby learning experience rather than a new super serious industry standard, plus the architecture borrows many existing concepts from the most popular and iconic ISAs like x86, RISC-V, MIPS, ARM and openRISC. Don't expect this project to be as good or polished as the commercial ones, however, i wanted to design something that can be considered decent and that goes beyond simpler toy architectures used for teaching. I chose to target FPGAs as the primary platform for two main reasons: firstly is that ASICs are out of the question for me and most people because of cost and required expertise. Second is that using discrete components makes little sense from a sanity and practicality point of view given the complexity of the project, however, software simulators can be a good platform for simpler implementations without spending a dime.
    \vspace{10pt}\hfill\break
    The core ideas here are the use of variable length encoding of 4 and 6 byte instruction sizes along with shorter ``compressed'' ones to increase code density. The goal behind these lengths are to use the long 6 byte format for instructions containing larger immediates, while the 2 and 4 byte formats for instructions with smaller immediates or registers only. Many of the longer formats have a shorter ``sibling'' to reach appropriate code densities. The trade off of this is increased ISA capabilities and code density at the expense of misalignment and variable length which complicates the hardware a bit. This ISA, all though not a ``pure'' RISC design with basic instructions and few addressing modes, resembles that philosophy for the most part skewing away from it in some areas, such as, being able to load, store, move and swap multiple registers with a single instruction, more complex addressing modes, floating point transcendental operations and variable length encodings.
    \vspace{10pt}\hfill \break
    I chose the name ``FabRISC'' because i wanted to encapsulate the main characteristics and target device of this instruction set. The pronunciation should vaguely remind of the word ``fabric'' which is a reference on the fact that the main component of an FPGA is the ``LUT fabric'', that is, a network of many interconnected logic cells.}
    \par\noindent\rule{\textwidth}{0.4pt}

    \subsection[Terminology]{\Large Terminology}

        \vspace{10pt}

        The FabRISC architecture uses the following terminology throughout the document in order to more accurately define technical concepts and vocabulary:

        \begin{enumerate}[label=\arabic*)]

            \item \textit{\textbf{``Abort''} and \textbf{``abortion''} are used to refer to the act of abruptly stopping an ongoing memory transaction as well as invalidating all of its changes.}

            \item \textit{\textbf{``Architecture''} is used to refer to the set of abstractions that the hardware provides to the software.}

            \item \textit{\textbf{``Atomic''} is used to refer to any operation that must, either be completely executed, or not at all.}

            \item \textit{\textbf{``Architectural state''} is used to refer to the state of the processor, or a single core or hardware thread, that can directly be observed by the programmer.}

            \item \textit{\textbf{``Coherence''} is used to refer to the ability of a system to be coherent, that is, ensuring the uniformity of shared resources across the entire system. In particular, it defines the ordering of accesses to a single memory location that is obeyed by everyone within the system.}

            \item \textit{\textbf{``Consistency''} is used to refer to the ability of a system to be consistent, that is, defining a particular order of operations across all memory locations that is obeyed by everyone within the system.}

            \item \textit{\textbf{``Consistency model''} is used to refer to a particular model or protocol of consistency within a particular system.}

            \item \textit{\textbf{``Core''} is used to refer to a fully functional and complete sub-CPU within a bigger entity. Advanced processors often aggregate multiple similar sub-CPUs in order to be able to schedule different programs each working on it's own stream of data. It is important to note that each core can implement a completely different microarchitecture, as well as, instruction set.}

            \item \textit{\textbf{``Event''} is used to generically refer to any extra-ordinary situation that needs to be taken care of as soon as possible.}

            \item \textit{\textbf{``Exception''} is used to refer to any non severe internal, synchronous event.}

            \item \textit{\textbf{``Fault''} is used to refer to any severe internal, synchronous event.}

            \item \textit{\textbf{``Hardware thread''}, \textbf{``hart''} or simply \textbf{``logical core''} are used to refer to a particular physical instance of a software thread running, specifically, on the central processing unit (CPU).}

            \item \textit{\textbf{``Instruction set architecture''} or simply \textbf{``ISA''} are used to refer to the architecture that the central processing unit provides to the software under the form of instructions, registers and other resources.}

            \item \textit{\textbf{``Interrupt''} is used to refer to any external, asynchronous event.}

            \item \textit{\textbf{``Macro-operation''} or simply \textbf{``macro-op''} are synonyms of ``instruction''.}

            \item \textit{\textbf{``Memory fence''} or simply \textbf{``fence''} are used to refer to particular instructions that have the ability to enforce a specific ordering of other memory instructions.}

            \item \textit{\textbf{``Memory transaction''} or simply \textbf{``transaction''} are used to refer to a particular series of operations that behave atomically within the system.}

            \item \textit{\textbf{``Micro-operation''} or simply \textbf{``micro-op''} are used to refer to a partially or fully decoded instruction.}

            \item \textit{\textbf{``Microarchitectural state''} is used to refer to the actual state of the processor, or a single core or hardware thread, that might not be visible by the programmer in its entirety.}

            \item \textit{\textbf{``Microarchitecture''} is used to refer to the particular physical implementation of a given architecture.}

            \item \textit{\textbf{``Notification''} is used to refer to any internal, asynchronous event.}

            \item \textit{\textbf{``Page''} is used to refer to a logical partition of the main system memory.}

            \item \textit{\textbf{``Promotion''} is used to refer to the automatic switch from user mode to supervisor mode by the processor or a single core or hardware thread, caused by specific events such as interrupts and faults.}

            \item \textit{\textbf{``Transparent''} is used to refer to something that is, mostly, invisible to the programmer.}

            \item \textit{\textbf{``Trap''} is used to refer to the transition from a state of normal execution to the launch of an event handler.}

            \item \textit{\textbf{``Unaligned''} or \textbf{``misaligned''} are used to refer to any memory item that is not naturally aligned, that is, the address of the item modulo its size, is not equal to zero.}

        \end{enumerate}

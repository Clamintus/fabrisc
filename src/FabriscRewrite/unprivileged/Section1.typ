== #underline("Introduction")
\
This is the official document for the unprivileged FabRISC instruction set architecture. This writing is intended to introduce and explain the basic concepts and core capabilities of the ISA. 

FabRISC is a feature rich, register-register, classically virtualizable, load-store architecture with variable length encodings of 2, 4 and 6 bytes. This specification is designed to be highly modular allowing simple and straightforward implementations from basic designs up to high performance systems by picking and choosing only the desired functionalities. The ISA includes scalar, vector, floating-point, compressed as well as atomic memory instructions capable of supporting 32 and 64-bit multithreaded microarchitectures and, with more basic implementations, it's also possible to realize processors of 8 and 16-bit widths as well. The complete specification also includes privileged and system related instructions, interrupts, exceptions, faults and more which help support the realization of various kinds of operative systems with or without memory management needs (see the privileged spec). The ISA can be further expanded and enriched by allocating unused encoding space to custom application specific instructions, thus increasing both performance and code density. FabRISC is completely free, open-source and available to anyone interested in the project via the official GitLab page: #link("https://gitlab.com/Clamintus/fabrisc") (license details can be found at the very end of this document). The specification is divided into multiple sections each explaining a particular aspect of the architecture in detail with the help of tables, figures and suggestions in order to aid the hardware and software designers in creating an efficient realization of the FabRISC instruction set architecture.

//__________________________________________________________________________________________________________________________________________

#line(length: 100%)
#emph([

    Commentary in this document will formatted in this way and communication will be more colloquial. If the reader is only interested in the specification, these sections can be skipped without hindering the understanding of the document.

    I decided to split the privileged and unprivileged specification in two separate documents for better decoupling. This is mainly to make changes on one specification be more independent of the other, as well as to help me getting things done more quickly.

    This project tries to be more of a hobby learning experience rather than a new super serious industry standard, plus the architecture borrows many existing concepts from the most popular and iconic ISAs like x86, RISC-V, MIPS, ARM and openRISC. Don't expect this project to be as good or polished as the commercial ones, however, i wanted to design something that can be considered decent and that goes beyond simpler toy architectures used for teaching. I chose to target FPGAs as the primary platform for two main reasons: firstly is that ASICs are out of the question for me and most people because of cost and required expertise. Second is that using discrete components makes little sense from a sanity and practicality point of view given the complexity of the project, however, software simulators can be a good platform for simpler implementations without spending a dime.

    The core ideas here are the use of variable length encoding of 4 and 6 byte instruction sizes along with shorter "compressed" ones to increase code density. The goal behind these lengths are to use the long 6 byte format for instructions containing larger immediates, while the 2 and 4 byte formats for instructions with smaller immediates or registers only. Many of the longer formats have a shorter "sibling" to reach appropriate code densities. The trade off of this is increased ISA capabilities and code density at the expense of misalignment and variable length which complicates the hardware a bit. This ISA, all though not a "pure" RISC design with basic instructions and few addressing modes, resembles that philosophy for the most part skewing away from it in some areas, such as, being able to load, store, move and swap multiple registers with a single instruction, more complex addressing modes, floating point transcendental operations and variable length encodings.

    I chose the name "FabRISC" because i wanted to encapsulate the main characteristics and target device of this instruction set. The pronunciation should vaguely remind of the word "fabric" which is a reference on the fact that the main component of an FPGA is the "LUT fabric", that is, a network of many interconnected logic cells.
])
#line(length: 100%)

//__________________________________________________________________________________________________________________________________________

== Terminology
\
The FabRISC architecture uses the following terminology throughout the document in order to more accurately define technical concepts and vocabulary:

    + *"Abort"*, *"abortion"* _and_ *"Transaction abort"* _are used to refer to the act of abruptly stopping an ongoing memory transaction as well as invalidating (rolling back) all of its changes._

    + *"Architecture"* _is used to refer to the set of abstractions that the hardware provides to the software._

    + *"Atomic"* _is used to refer to any operation that must, either be completely executed, or not at all._

    + *"Architectural state"* _is used to refer to the state of the processor, or a single core or hardware thread, that can directly be observed by the programmer._

    + *"Coherence"* _is used to refer to the ability of a system to be coherent, that is, ensuring the uniformity of shared resources across the entire system. In particular, it defines the ordering of accesses to a single memory location that is obeyed by everyone within the system._

    + *"Consistency"* _is used to refer to the ability of a system to be consistent, that is, defining a particular order of operations across all memory locations that is obeyed by everyone within the system._

    + *"Consistency model"* _is used to refer to a particular model or protocol of consistency within a particular system._

    + *"Core"* _is used to refer to a fully functional and complete sub-CPU within a bigger entity. Advanced processors often aggregate multiple similar sub-CPUs in order to be able to schedule different programs each working on it's own stream of data. It is important to note that each core can implement a completely different microarchitecture, as well as, instruction set._

    + *"Event"* _is used to generically refer to any extra-ordinary situation that needs to be taken care of as soon as possible._

    + *"Exception"* _is used to refer to any non severe internal, synchronous event._

    + *"Fault"* _is used to refer to any severe internal, synchronous event._

    + *"Hardware thread"*, *"hart"* _or simply_ *"logical core"* _are used to refer to a particular physical instance of a software thread running, specifically, on the central processing unit (CPU)._

    + *"Instruction set architecture"* _or simply_ *"ISA"* _are used to refer to the architecture that the central processing unit provides to the software under the form of instructions, registers and other resources._

    + *"Interrupt"* _is used to refer to any external, asynchronous event._

    + *"Macro-operation"* _or simply_ *"macro-op"* _are synonyms of_ *"instruction"*.

    + *"Memory fence"* _or simply_ *"fence"* _are used to refer to particular instructions that have the ability to enforce a specific ordering of other memory instructions._

    + *"Memory transaction"* _or simply_ *"transaction"* _are used to refer to a particular series of operations that behave atomically within the system._

    + *"Micro-operation"* _or simply_ *"micro-op"* _are used to refer to a partially or fully decoded instruction._

    + *"Microarchitectural state"* _is used to refer to the actual state of the processor, or a single core or hardware thread, that might not be visible by the programmer in its entirety._

    + *"Microarchitecture"* _is used to refer to the particular physical implementation of a given architecture._

    + *"Notification"* _is used to refer to any internal, asynchronous event._

    + *"Page"* _is used to refer to a logical partition of the main system memory._

    + *"Promotion"* _is used to refer to the automatic switch from user mode to supervisor mode by the processor or a single core or hardware thread, caused by specific events such as interrupts and faults._

    + *"Transparent"* _is used to refer to something that is, mostly, invisible to the programmer._

    + *"Trap"* _is used to refer to the transition from a state of normal execution to the launch of an event handler._

    + *"Unaligned"* _or_ *"misaligned"* _are used to refer to any memory item that is not naturally aligned, that is, the address of the item modulo its size, is not equal to zero._
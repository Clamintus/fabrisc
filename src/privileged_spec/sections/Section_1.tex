\section[Events]{\huge\underline{Events}}

    \vspace{10pt}

    This section is dedicated to the specification of exceptions, faults and interrupts which are needed to implement the EXC, FLT, IOINT, IPCINT, DBGR, PERFC, DALIGN, IALIGN and SUPER modules. FabRISC uses the term \textit{event} to indicate the generic categorization of these kinds of situations. Events can be used to communicate with other harts, processes, IO devices, signal system faults or simply trigger software exceptions. Events are sub divided into two main categories: Synchronous and Asynchronous.

    \subsection[Synchronous events]{\Large Synchronous events}

        \vspace{10pt}

        Synchronous events are internally generated and are considered deterministic, that is, if they happen, they will always happen in the same location of the executing program given the same initial state and, because of this, the handling must done in program order unless noted otherwise. This category is further sub divided in two:

        \begin{itemize}

            \item \textit{\textbf{Exceptions}: these events are user or supervisor handled, non promoting and with a global priority level of 1. They are generated by the executing instructions and each process can have it's own private handler with the help of a dedicated pointer register (UEHP). From a higher level, the handling of exceptions looks like an automatic function call to the specified handler location all within the address space of the process in question.}

            \item \textit{\textbf{Faults}: these events are supervisor only, promoting and with a global priority level of 4. They are generated by the executing instructions and each process will have the same handler dictated by the supervisor (SEHP). From a high level, the handling of faults looks like an automatic function call performed by the supervisor to a location that is always the same and part of the OS code.}

        \end{itemize}

    \par\noindent\rule{\textwidth}{0.4pt}
    \textit{Exceptions and faults are used to identify software related problems and edge cases such as overflows, divisions by zero, memory accesses to protected sections and more. I believe that exceptions can be valuable and useful for handling special cases quickly and preemptively without having to perform time consuming checks over and over. Exceptions can be enabled by operating the hart in a ``safe state'', which will trigger the appropriate exception every time something went wrong with the executing software. Faults are similar to exceptions but for more delicate problems such as page faults, illegal or malformed instructions and accesses to protected memory areas. All in all, i believe that one cannot really design a hardware system where these kinds of problems never occur, which is why i put emphasis on being able to gracefully recover when such cases happen.}
    \par\noindent\rule{\textwidth}{0.4pt}

    \subsection[Asynchronous events]{\Large Asynchronous events}

        \vspace{10pt}

        Asynchronous events are externally generated and are not considered deterministic, that is, they can happen at any time regardless of what the CPU is doing. These events should be handled as soon as possible in order to keep latency low. This category is further sub divided in three:

        \begin{itemize}

            \item \textit{\textbf{Notifications}: these events are user or supervisor handled, non promoting with a global priority of 0. They are generated internally by the hart itself from the debugging registers and performance counters and each process can have it's own private handler with the help of a dedicated pointer register (UEHP). From a higher level, the handling of notifications looks like an automatic function call to the specified handler location all within the address space of the process in question in a similar fashion to exceptions and transaction aborts.}

            \item \textit{\textbf{Transaction aborts}: these events are user or supervisor handled, non promoting with a global priority of 2. They can be generated internally by the hart itself or by other harts that invalidate the ongoing transaction. Aborts are handled in a different manner than other events because, when a transaction is active, it changes the way the hart behaves needing a special way of triggering and handling these events depending on the situation. More information can be found in the next subsections below.}

            \item \textit{\textbf{IPC interrupts}: These events are supervisor only, promoting with a global priority level of 3. They are generated by other harts in the system in order to communicate with each other (inter-process communication) and can have a dynamic internal priority level to decide which one to handle when multiple are triggered at the same time. From a high level, the handling of IPC interrupts, can be considered as a context switch to the desired process that will handle the IPC request, which can be achieved in a similar fashion to IO interrupts.}

            \item \textit{\textbf{IO interrupts}: these events are supervisor only, promoting with a global priority level of 5. They are generated by external IO devices and can have a dynamic internal priority level to decide which one to handle when multiple are triggered at the same time. From a high level, the handling of IO interrupts, can be considered as a context switch to the desired process that will handle the device request.}

        \end{itemize}

    \par\noindent\rule{\textwidth}{0.4pt}
    \textit{I think that interrupts are a great tool that allow the IO devices themselves to start the communication, as opposed to the time consuming polling. This option is especially useful when low latency is required and can be used in conjunction with the regular low speed memory mapped IO transfers or the faster DMA. For devices that do not need any kind of bandwidth or responsiveness, polling can still be a valid choice without utilizing any extra resources. A shadow register file can potentially be utilized to bring latency to a minimum, however, it must behave transparently to the architecture. IPC interrupts are intended for communication between different processes or threads. Conceptually, they work in a similar fashion to IO interrupts with the only difference being that IPC interrupts are generated by other processes or threads and have a lower innate priority. There are also a more special purpose version of interrupts that can be used by the user to react to different events coming from debugging registers and performance counters.}
    \par\noindent\rule{\textwidth}{0.4pt}

    \subsection[Event modules requirements]{\Large Event modules requirements}

        \vspace{10pt}

        In order to be able to handle any kind of event, the hardware must provide the appropriate resources such as registers or instructions. User and supervisor events will have dedicated and separate registers that can be used during the handling process. If multiple event modules are implemented, multiple instances of the same requirements are, obviously, needed just once. A list containing all the possible events is provided at the bottom of this section.

        \begin{itemize}

            \item \textit{\textbf{EXC module}: this module adds exceptions to the architecture. The hardware must implement the CAUSE, UPCB, UCSRB and UCAUSEB registers that are used as buffers when an exception is triggered along with the ERET instruction (see section 9 for more information) and the actual exceptions. If the SUPER module is present, then the SPCB, SCSRB and SCAUSEB registers must also be included.}

            \item \textit{\textbf{FLT module}: this module adds faults to the architecture. The hardware must implement the CAUSE, SPCB, SCSRB and SCAUSEB registers that are used as buffers when an exception is triggered along with the ERET instruction (see section 9 for more information) and the actual faults. This module requires the presence of the SUPER module.}

            \item \textit{\textbf{DALIGN module}: this module adds the MISA fault to the architecture. The hardware must implement the CAUSE, SPCB, SCSR and SCAUSEB registers that are used as buffers when the mentioned fault is triggered along with the ERET instruction (see section 9 for more information). This module is only mandatory when the system doesn't support misaligned accesses on data.}

            \item \textit{\textbf{IALIGN module}: this module adds the MISI fault to the architecture. The hardware must implement the CAUSE, SPCB, SCSR and SCAUSEB registers that are used as buffers when the mentioned fault is triggered along with the ERET instruction (see section 9 for more information). This module is only mandatory when the system doesn't support misaligned accesses on instructions.}

            \item \textit{\textbf{IOINT module}: this module adds IO interrupts to the architecture. The hardware must implement the CAUSE, SPCB, SCSRB and SCAUSEB registers that are used as buffers when an IO interrupt is triggered along with the ERET instruction (see section 9 for more information) and the actual IO interrupts. This module requires the presence of the SUPER module.}

            \item \textit{\textbf{IPCINT module}: this module adds IPC interrupts to the architecture. The hardware must implement the CAUSE, SPCB, SCSRB and SCAUSEB registers that are used as buffers when an IPC interrupt is triggered along with the ERET instruction (see section 9 for more information) and the actual IPC interrupts. This module requires the presence of the SUPER module.}

            \item \textit{\textbf{DBGR module}: this module adds some notifications to the architecture. The hardware must implement the CAUSE, UPCB, UCSRB and UCAUSEB registers that are used as buffers when a notification is triggered along with the ERET instruction, debugging registers (see section 8 and 9 for more information) and the actual notifications. If the SUPER module is present, then the CAUSE, SPCB, SCSRB and SCAUSEB registers must also be included.}

            \item \textit{\textbf{PERFC module}: this module adds some notifications to the architecture. The hardware must implement the CAUSE, UPCB, UCSRB and UCAUSEB registers that are used as buffers when a notification is triggered along with the ERET instruction, performance counters (see section 8 and 9 for more information) and the actual notifications. If the SUPER module is present, then the CAUSE, SPCB, SCSRB and SCAUSEB registers must also be included.}

            \item \textit{\textbf{SUPER module}: this module gives the hart the ability to define protected resources such as memory addresses, registers, instructions as well as requiring the SWITCH IO interrupt. Events that are deemed promoting, rely on the presence of this module. Other requirements and information for this module can be found in sections 7, 8 and 9.}

        \end{itemize}

        \noindent If the supervisor (SUPER module) is not decided to be implemented, events can still be realized without it. The purpose of the supervisor is to provide a barrier between the user address space and the OS address space via the use of an MMU. Without the supervisor, context switches and promotions are not necessary anymore alleviating the requirements for simpler machines such as micro controllers which don't need or require any privileged ISA capability because they, usually, do not run any operative system. Some events of the FLT module are mandatory, specifically the ILLI, MALI and INCI events must always be implemented in order to provide handling in critical situations. A simple way would be to hard-wire the handler address, removing the need for any privileged register and instruction. The same picture can be painted for the DALIGN and IALIGN modules which, as mentioned above, are only necessary when the microarchitecture doesn't support misaligned accesses to data and instructions respectively.

    \subsection[Event handling]{\Large Event handling}

        \vspace{10pt}

        Handling events involve a \textit{``launching''} phase and a \textit{``returning''} phase. The steps of each phase must be performed by the trapped hart in a single cycle in order to avoid potential corruption of its internal state. The following is the series of steps to launch and return to and from the exception handler which depend on the triggered event as well as current privilege.

        \subsubsection[Launching phase]{Launching phase}

            \vspace{10pt}

            The launching phase is composed of the following steps that must be performed in order:

            \begin{enumerate}[label=\arabic*)]

                \item \textit{write the program counter (PC) into the appropriate buffer register depending on the privilege:}

                \begin{itemize}

                    \item \textit{if the machine is in user mode and the event is non promoting: write the PC into the UPCB register.}
                    
                    \item \textit{if the machine is in supervisor mode and the event is non promoting: write the PC into the SPCB register.}

                    \item \textit{if the machine is in supervisor mode and the event is promoting: write the PC into the SPCB register.}

                \end{itemize}

                \item \textit{if the event is promoting and there are active memory transactions:}

                \begin{itemize}

                    \item \textit{abort all ongoing memory transactions.}
                    \item \textit{set the TAD field of the CSR to all zeros.}
                    \item \textit{set the TAC field of the CSR to the id of the EABT abort (0001).}
                    \item \textit{set the UTRAP bit of the CSR to 1.}

                \end{itemize} 

                \item \textit{write the control and status register (CSR) into the appropriate buffer register depending on the privilege:}

                \begin{itemize}

                    \item \textit{if the machine is in user mode and the event is non promoting: write the CSR into the UCSRB register.}

                    \item \textit{if the machine is in supervisor mode and the event is non promoting: write the CSR into the SCSRB register.}

                    \item \textit{if the machine is in supervisor mode and the event is promoting: write the CSR into the SCSRB register.}

                \end{itemize}

                \item \textit{write the event cause register (CAUSE) into the appropriate buffer register depending on the privilege:}

                \begin{itemize}

                    \item \textit{if the machine is in user mode and the event is non promoting: write the CAUSE register into the UCAUSEB \item register.}

                    \textit{if the machine is in supervisor mode and the event is non promoting: write the CAUSE register into the SCAUSEB register.}

                    \item \textit{if the machine is in supervisor mode and the event is promoting: write the CAUSE register into the SCAUSEB register}

                \end{itemize}

                \item \textit{write the extra event information in the event cause register (CAUSE) depending on the triggered event.}

                \item \textit{write in the CSR the appropriate settings:}

                \begin{itemize}

                    \item \textit{if the machine is in user mode and the event is non promoting:}

                    \begin{enumerate}[label=\arabic*)]

                        \item \textit{write the event id into the EID bits.}
                        \item \textit{set the NTFM bit to 1.}
                        \item \textit{set the TRAP bit to 1.}

                    \end{enumerate}

                    \textit{if the machine is in supervisor mode and the event is non promoting:}

                    \begin{enumerate}[label=\arabic*)]

                        \item \textit{write the event id into the EID bits.}
                        \item \textit{set the NTFM bit to 1.}
                        \item \textit{set the TRAP bit to 1.}
                        \item \textit{set the DBGF bit to 1.}
                        \item \textit{set the CNTF bit to 1.}
                        \item \textit{set the IPCM bit to 1.}
                        \item \textit{set the IOIM bit to 1.}

                    \end{enumerate}

                    \item \textit{if the machine is in supervisor mode and the event is promoting:}

                    \begin{enumerate}[label=\arabic*)]

                        \item \textit{write the event id into the EID bits.}
                        \item \textit{set the NTFM bit to 1.}
                        \item \textit{set the TRAP bit to 1.}
                        \item \textit{set the DBGF bit to 1.}
                        \item \textit{set the CNTF bit to 1.}
                        \item \textit{set the IPCM bit to 1.}
                        \item \textit{set the IOIM bit to 1.}
                        \item \textit{set the SUPB bit to 1.}

                    \end{enumerate}

                \end{itemize}

                \item \textit{jump to the handler address by writing into the PC the appropriate event handler pointer depending on the privilege:}

                \begin{itemize}

                    \item \textit{if the machine is in user mode and the event is non promoting: write the UEHP register into the PC.}
                    
                    \item \textit{if the machine is in supervisor mode and the event is non promoting: write the SEHP register into the PC.}
                    
                    \item \textit{if the machine is in supervisor mode and the event is promoting: write the SEHP register into the PC.}

                \end{itemize}

            \end{enumerate}

        \subsubsection[Returning phase]{Returning phase}

            \vspace{10pt}

            The returning phase is composed of the following steps that must be performed in order:

            \begin{enumerate}[label=\arabic*)]

                \item \textit{write the appropriate event buffer register into the cause register (CAUSE) depending on the privilege:}

                \begin{itemize}

                    \item \textit{if the machine is in user mode the ERET instruction is executed: write the UCAUSEB register into the CAUSE register.}

                    \item \textit{if the machine is in supervisor mode the ERET instruction is executed: write the SCAUSEB register into the CAUSE register.}

                    \item \textit{if the machine is in supervisor mode the URET instruction is executed: write the SCAUSEB register into the CAUSE register.}

                \end{itemize}

                \item \textit{write the appropriate event buffer register into the control and status register (CSR) depending on the privilege:}

                \begin{itemize}

                    \item \textit{if the machine is in user mode the ERET instruction is executed: write the UCSRB register into the CSR register.}

                    \item \textit{if the machine is in supervisor mode the ERET instruction is executed: write the SCSRB register into the CSR register.}

                    \item \textit{if the machine is in supervisor mode the URET instruction is executed: write the SCSRB register into the CSR register.}

                \end{itemize}

                \item \textit{write the appropriate event buffer register into the program counter (PC) depending on the privilege:}

                \begin{itemize}

                    \item \textit{if the machine is in user mode the ERET instruction is executed: write the UPCB register into the PC register.}

                    \item \textit{if the machine is in supervisor mode the ERET instruction is executed: write the SPCB register into the PC register.}

                    \item \textit{if the machine is in supervisor mode the URET instruction is executed: write the SPCB register into the PC register.}

                \end{itemize}

            \end{enumerate}

    \subsection[Notes on event handling]{Notes on event handling}

        \vspace{10pt}

        During the launching phase, as mentioned in the appropriate subsections, certain events will be masked in order to avoid being trapped again and corrupt the state. If nesting is desired, the handler must at least save the appropriate ``critical'' registers (program counter, control and status register and cause register buffers) to memory. From this point, the handler is free to lift any mask ready to be triggered again. Since nesting of events is mostly manual, re-triggering another event before saving the critical registers to memory by either removing the masks for interrupts or generating another fault or exception, will result in the corruption of said registers. Returning from the handler will, thus, be impossible because the critical registers have been overwritten. FabRISC calls this situation the \textit{double event} and is going to be discussed shortly in more details.
        \vspace{10pt}\break
        Promotions from user to the supervisor are an exception to this rule and are always allowed at any cycle even before saving the user critical registers to memory. This is because privileged events use a different set of buffer registers, thus leaving the user ones untouched.
        \vspace{10pt}\break
        It is important to note that masked events are still allowed to be generated, but they are not allowed to trap the hart if masked, thus necessitating of a queuing system. Some events that don't carry any extra information might even be merged together to save space in the queues too (see the tables at the bottom of this section).
        \vspace{10pt}\break
        Global priority, introduced in the previous subsections, is used to give a further discriminant in case different types of events are triggered in the same clock cycle. Events with global priority level of 4 will be considered first, all the way to global priority level 0. Local priority simply specifies the order in which events of the same type must be handled and only concerns asynchronous events. Local priority for interrupts can be dynamically changed by the supervisor with the help of the IO controller, which should be responsible for scheduling the actual interrupt to the target hart. It is important to note that it is forbidden to have more than one IO or IPC interrupt with the same local priority. This is because there shouldn't be any ambiguity on the scheduling order. Synchronous events do not have local priority because those events are generated from the instructions themselves and should, therefore, be handled in program order naturally satisfying any ambiguity.
        \vspace{10pt}\break
        Performance counters and debugging registers, if present and active, must be freezed if the triggered event is promoting, such as a fault or interrupt, which is accomplished via the DBGF and the CNTF bits as mentioned in the launching phases. If the supervisor needs said resources, it must save them to memory first before utilizing them. The supervisor must restore the proper values back before actually scheduling a user process for execution like any other register, which ensures the retention of values across context switches.
        \vspace{10pt}\break
        Active transactions, as mentioned in the launching phase, must be aborted if a promoting event, such as faults and interrupts, are triggered regardless of the privilege. This is simply because the context is switched or a more severe problem is detected and continuing in transactional mode makes little sense and can potentially disturb the transactions of other harts. This is accomplished by immediately aborting all transactions of the hart and asserting the TAT bit to 1, the TAD to all zeros and writing the TAC bits with the identifier of the EABT abort code in the CSR before saving it in the appropriate buffer. This will signal to the returning process that its transactions failed because of an interrupt or fault and must immediately jump to the abort handler.
        \vspace{10pt}\break
        Handling aborts normally, however, simply consists in writing the TAT bit to 1, the TAD with the depth of the aborted transaction, the TAC field with the abort code and jumping to the handler pointed by the AHP register. Returning from the abort handler is always accomplished by writing the AHRA and AHRCSR back into the PC and CSR respectively. It is important to remember that AHP, AHRA and AHRCSR are set when the transaction starts by the TBEG instruction.
        \vspace{10pt}\break
        Nesting aborts might be needed if multiple nested transactions are active. This can be safely accomplished by saving the AHRA and AHRCSR in memory before starting a nested transaction. The abort handler can be the same for all the levels thus avoiding saving and restoring AHP as well. The latest handler, when returning, can then load the appropriate AHRA and AHRCSR depending on the depth level of the aborted transaction. With this, an abort handler will only preempt another if and only if an outer transaction failed, thus cancelling all the inner ones. It is important to note that it doesn't matter if the preempted abort handler never completes because the outer handler will abort everything anyways.

    \subsection[Double event]{\Large Double event}

        \vspace{10pt}

        During the handling of an event, the re-triggering of another event before saving the critical registers to memory will result in the corruption of state because those registers will be overwritten. The double event can happen when the privileges stay the same:

        \begin{enumerate}[label=\arabic*)]

            \item \textit{the user is trapped by a user level event. This causes the UPCB, UCSRB and UCAUSEB to be overwritten.}
            \item \textit{the supervisor is trapped by a supervisor level event. This causes the SPCB, SCSRB and SCAUSEB to be overwritten.}

        \end{enumerate}

        \noindent As mentioned earlier, promotions will never result in the double event because the PC, CSR and CAUSE registers will be saved in the SPCB, SCSRB and SCAUSEB without changing the state of the UPCB, UCSRB and UCAUSEB registers.
        \vspace{10pt}\break
        The double event, for asynchronous type events, can be avoided by simply unmasking after the critical registers have been saved to memory. For synchronous type events, the handler must be designed and programmed correctly in order to not generate other exceptions or faults. The AEXC bit, which suppresses any arithmetic exception, can be used when the handler must perform instructions that can generate such events before saving the critical registers. The hardware, however, must be able to detect the generation of the \textit{user double event} by triggering the UDE fault in response.
        \vspace{10pt}\break
        The only possible way to create a problematic double event is when a fault such as ILLI, MALI, MISI, INCI, MISA or PGFT are, once again, triggered before the critical registers are saved to memory during the handling of a supervisor level event. If this occurs, the hart must be immediately halted and the SDE bit in the CSR must be set to 1 in order to make the rest of the system aware of the situation. Avoiding this comes down to program the supervisor event handler free of illegal, malformed, misaligned or incompatible instructions. The page fault can be easily avoided by leaving the handler always loaded in memory, thus completely eliminating the change of triggering the PGFT event. \vspace{10pt}

    \par\noindent\rule{\textwidth}{0.4pt}
    \textit{Handling events should be quite a straight forward process. The launching phases explains all the needed steps to successfully branch to the target location. Once the handler is performed the ERET instruction can be executed to bring back the old state. This mechanism can be used to schedule another process entirely, by setting all the registers to the desired values and executing the URET instruction which causes the supervisor event buffers to be written into the PC, CSR and CAUSE registers. When an event of any kind is triggered, it's necessary to flush or invalidate any in-flight instruction. Events are not perfect mechanisms and can result in the awful double event situation. Luckily this situation can only arise if the handler code isn't coded correctly and, if it happens in user mode, it can be caught by the supervisor and the faulty process or thread can be terminated. More concerning is when the double event is triggered in supervisor mode, which signals that the supervisor handler code is itself faulty and the hart must be immediately halted. This can be considered a non issue because it can be easily avoided by properly writing the handler and always keeping it loaded in memory at all times.}
    \par\noindent\rule{\textwidth}{0.4pt}

    \clearpage
    \input{./Tables/User_event_table.tex}
    \clearpage
    \input{./Tables/Supervisor_event_table.tex}
    \vspace{10pt}